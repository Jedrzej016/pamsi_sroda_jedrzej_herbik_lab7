#include<iostream>
#include <string>
#include "AVL.hh"

using namespace std;

int dl_kolejki=0;

int main(int argc, char *argv[]){
  BinaryTree<int> drzewo;
  cout<<endl;
  for(int i=0;i<10;i++){
    cout<<"Dodano: "<<drzewo.dodaj(i%5);
    cout<<" "<<"Wyskokosc drzewa binarnego: "<<drzewo.wysokosc()<<endl;
  }
  //  dodanie do drzewa elementow 0 1 2 3 4 5 0 1 2 3 4 5 3

  cout<<endl;
  drzewo.dodaj(3);
  drzewo.preorder();
  drzewo.postorder();
  drzewo.inorder();
  cout<<endl;
  cout<<endl;
  drzewo.screen();
  cout<<endl;
  
  cout<<"Zawartosc korzenia drzewa binarnego: "<<drzewo.getRoot()<<endl;
  cout<<endl<<"Wyskokosc drzewa binarnego: "<<drzewo.wysokosc()<<endl<<endl;  
  //  usuniecie elementw drzewa wyswietlajac jego wysokosc i element usuwany
  while(!drzewo.isEmpty()){
    try{
      cout<<"Usunieto: "<<drzewo.usun();
    }catch(TreeEmptyException){
      cout<<"Drzewo jest Puste"<<endl;
    }
    if(drzewo.wysokosc()>=0)cout<<" "<<"Wyskokosc drzewa binarnego: "<<drzewo.wysokosc()<<endl;
    else cout<<" Drzewo jest puste"<<endl;
  }
  drzewo.rozmiar();
  if(!drzewo.clear())cout<<"Nie zwolniono calej pamieci"<<endl;
  cout<<endl;
  return 0;
}
