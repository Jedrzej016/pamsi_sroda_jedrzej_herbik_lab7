#include<cstdlib>
#include<ctime>

#define WEZLY 2 // ilosc galezi z wezla

class TreeEmptyException{};
extern int dl_kolejki; // zmienna do okreslania przecieku pamieci

template <class Object>
class wezelDrzewa{
protected:
  Object zawartosc;    // dane
  int wysokosc; //  okresla wysokosc na jakiej znajduje sie element
  wezelDrzewa<Object> *rodzic; // wskaznik do przodka
  wezelDrzewa<Object> *syn[WEZLY]; // wskazniki na galezie
public:
  wezelDrzewa(){rodzic=NULL;for(int i=0;i<WEZLY;i++){syn[i]=NULL;wysokosc=1;}}
  Object& getZawartosc(){return zawartosc;}
  Object& setZawartosc(const Object &dane){zawartosc=dane;return zawartosc;}
  int& getWysokosc(){return wysokosc;}
  int& setWysokosc(const int &dane){wysokosc=dane;return wysokosc;}
  wezelDrzewa<Object>*& getSon(int i){return syn[i];}
  wezelDrzewa<Object>*& setSon(wezelDrzewa<Object> (*dane),int i){syn[i]=dane;return syn[i];}
  wezelDrzewa<Object>*& getParent(){return rodzic;}
  wezelDrzewa<Object>*& setParent(wezelDrzewa<Object> (*dane)){rodzic=dane;return rodzic;}
};
  
template <class Object>
class Tree{
protected:
  Object zawartosc;    // dane
  wezelDrzewa<Object> *root; // wskaznik do przodu  
  int dl;
  int galaz;
  int pokolenie;
  void preorder(wezelDrzewa<Object> *wezel);
  void postorder(wezelDrzewa<Object> *wezel);
  void inorder(wezelDrzewa<Object> *wezel);
  void clear(wezelDrzewa<Object> *wezel); //  czysci drzewo
  bool allIsNull(wezelDrzewa<Object> *wezel); //  zwraca true jezeli wszyscy synnowie sa rowni NULL
  int IsNull(wezelDrzewa<Object> *wezel);  // zwraca odpowiednio numer syna sprawdzajac czy synowie sa NULL
  int wysokosc(wezelDrzewa<Object>* wezel); //  zwraca wysokosc poddrzewa
  int mod(int a){if(a<0)return (-a);return a;} //  zwraca modol liczby a
  void screen(wezelDrzewa<Object> *wezel);
public:
  Tree();
  ~Tree(void){clear(root);}
  int rozmiar() const;
  bool isEmpty() const; 
  bool clear();         // wyczysc
  Object& getRoot()throw(TreeEmptyException);
  Object& dodaj(const Object& obj);
  Object& usun()throw(TreeEmptyException);
  int wysokosc();
  void screen();
  void preorder();
  void postorder();
  void inorder();
};

template <class Object>
Tree<Object>::Tree(){
  root=NULL;
  dl=0;
  pokolenie=0;
  galaz=0;
}

template <class Object>  // zwraca zawartosc korzenia
Object& Tree<Object>::getRoot()throw(TreeEmptyException){
  if(root!=NULL)return root->getZawartosc();
  throw TreeEmptyException();
}

template <class Object>  // wywolanie preorder
void Tree<Object>::screen(wezelDrzewa<Object> *wezel){
  if(wezel!=NULL){
    std::cout<<"zawartosc: "<<wezel->getZawartosc();
    std::cout<<std::endl;
    std::cout<<"Wysokowsc: "<<wezel->getWysokosc();  
    std::cout<<std::endl;
    if(wezel->getSon(0)!=NULL){
      std::cout<<"Lewy syn: ";this->screen(wezel->getSon(0));
      std::cout<<std::endl;
    }
    if(wezel->getSon(1)!=NULL){
      std::cout<<"Prawy syn: ";this->screen(wezel->getSon(1));
      std::cout<<std::endl;
    }
  }  
}

template <class Object>  // wywolanie preorder
void Tree<Object>::screen(){
  if(root!=NULL){
    std::cout<<"Korzen: "<<this->getRoot();
    std::cout<<std::endl;
    std::cout<<"Wysokowsc: "<<root->getWysokosc();  
    std::cout<<std::endl;
    if(root->getSon(0)!=NULL){
      std::cout<<"Lewy syn: ";this->screen(root->getSon(0));
      std::cout<<std::endl;
    }
    if(root->getSon(1)!=NULL){
      std::cout<<"Prawy syn: ";this->screen(root->getSon(1));
      std::cout<<std::endl;
    }
  }else std::cout<<"Nie istnieje"<<std::endl;
}

template <class Object>  // wywolanie preorder
void Tree<Object>::preorder(){
  std::cout<<"Przejscie Preorder: "<<std::endl;;
  preorder(root);
  std::cout<<std::endl;
}

template <class Object>  // rekurencyjne preorder
void Tree<Object>::preorder(wezelDrzewa<Object> *wezel){
  if(wezel!=NULL){
    std::cout<<wezel->getZawartosc()<<" ";
    for(int i=0;i<WEZLY;i++)preorder(wezel->getSon(i));
  }
}

template <class Object>  // wywolanie postorder
void Tree<Object>::postorder(){
  std::cout<<"Przejscie Postorder: "<<std::endl;;
  postorder(root);
  std::cout<<std::endl;
}

template <class Object>  // rekurencyjne postorder
void Tree<Object>::postorder(wezelDrzewa<Object> *wezel){
  if(wezel!=NULL){
    for(int i=0;i<WEZLY;i++)postorder(wezel->getSon(i));
    std::cout<<wezel->getZawartosc()<<" ";
  }
}

template <class Object>  // wywolanie inorder
void Tree<Object>::inorder(){
  std::cout<<"Przejscie Inorder: "<<std::endl;;
  inorder(root);
  std::cout<<std::endl;
}

template <class Object>  // rekurencyjne inorder
void Tree<Object>::inorder(wezelDrzewa<Object> *wezel){
  if(wezel!=NULL){
    for(int i=0;i<WEZLY;i++){
      inorder(wezel->getSon(i));
      if(i==0)std::cout<<wezel->getZawartosc()<<" ";
    }
  }
}

template <class Object>  // zwraca galaz czyli wysokosc drzewa
int Tree<Object>::wysokosc(){
  return wysokosc(root)-1;
}

template <class Object>  // zwraca galaz czyli wysokosc drzewa
int Tree<Object>::wysokosc(wezelDrzewa<Object>* wezel){
  if(wezel!=NULL)return (wezel->getWysokosc());
  return 0;
}

template <class Object>  // zwraca ilosc elementow
int Tree<Object>::rozmiar() const{
  return dl;
}

template <class Object>
bool Tree<Object>::isEmpty() const{
  if(dl||root) return false;
  return true;
}

template <class Object>  // wywolanie czyszczenia
bool Tree<Object>::clear(){
  clear(root);
  galaz=0;
  if((root!=NULL)||dl)return false;
  return true;
}

template <class Object>  // rekurencyjne czyszczenie
void Tree<Object>::clear(wezelDrzewa<Object> *wezel){
  if(wezel!=NULL){
    for(int i=0;i<WEZLY;i++)clear(wezel->getSon(i));
    delete wezel;
    if((wezel==root)&&(dl==1))root=NULL;
    dl--;
  }
}

template <class Object>                         // funkcja dodawania ktora zaslonimy w kolejnej klasie
Object& Tree<Object>::dodaj(const Object& obj){
  wezelDrzewa<Object> *wezel=NULL;
  wezelDrzewa<Object> *tmp_back=NULL;
  wezelDrzewa<Object> *tmp=root;
  int licznik=0;
  int wartosc=0;
  dl++;
  wezel=new wezelDrzewa<Object>;
  wezel->setZawartosc(obj);
  zawartosc=wezel->getZawartosc();
  while(tmp!=NULL){
    tmp_back=tmp;
    wartosc=std::rand()%WEZLY;
    tmp=tmp->getSon(wartosc);
  }
  wezel->setParent(tmp_back);
  if(tmp_back==NULL){
    root=wezel;
  }else{
    tmp_back->getSon(wartosc)=wezel;
  } //  koniec dodawania
  do{
    licznik++;
    tmp=tmp->getParent();
    if(licznik>tmp->getWysokosc())tmp->setWysokosc(licznik);
  }while(tmp==root); //  zapis wysokosci
  return zawartosc;
}

template <class Object>
bool Tree<Object>::allIsNull(wezelDrzewa<Object> *wezel){  // zwraca true jezeli wszyscy synowie sa NULL
  if(wezel!=NULL)for(int i=0;i<WEZLY;i++)if(wezel->getSon(i)!=NULL)return 0;
  return 1;
}

template <class Object>  // zwraca liczbe syna ktory nie jest NULL liczac od lewej do konca
int Tree<Object>::IsNull(wezelDrzewa<Object> *wezel){
  int zwrot=0;
  if(wezel!=NULL)for(int i=0;i<WEZLY;i++)if(wezel->getSon(i)!=NULL)zwrot=i;
  return zwrot;
}

template <class Object>  // usuwa element
Object& Tree<Object>::usun()throw(TreeEmptyException){
  int kierunek=0;
  wezelDrzewa<Object> *tmp=NULL;
  wezelDrzewa<Object> *tmp_back=NULL;
  wezelDrzewa<Object> *galaz1=NULL;
  wezelDrzewa<Object> *galaz2=NULL;
  wezelDrzewa<Object> *galaz3=NULL;
  wezelDrzewa<Object> *galaz4=NULL;
  Object pWartosc;
  galaz=0;
  while(root!=NULL){  // gdy istnieje tylko root
    if(allIsNull(root)){
      zawartosc=root->getZawartosc();
      delete(root);
      dl--;
      root=NULL;
      if(dl)throw TreeEmptyException();  // gdy licznik sie nie zgadza z iloscia elementow
      return zawartosc;
    }
    tmp=root;
    while(!allIsNull(tmp)){  // gdy sa jacys synowie
      tmp_back=tmp;
      tmp=tmp->getSon(IsNull(tmp_back));  // przeskakuje na syna najbardziej po prawej
      kierunek=IsNull(tmp_back);          // zpamietuje na ktorego syna przeskoczylo
    }
    zawartosc=tmp->getZawartosc();
    delete(tmp);                          // zwolnienie pamieci
    tmp_back->setSon(NULL,kierunek);      // czyszczenie syna
    dl--;
    while(tmp_back!=NULL){
      if((tmp_back->getSon(0)==NULL)&&(tmp_back->getSon(1)==NULL))tmp_back->setWysokosc(1);
      else if(tmp_back->getSon(0)==NULL)tmp_back->setWysokosc(tmp_back->getSon(1)->getWysokosc()+1);
      else if(tmp_back->getSon(1)==NULL)tmp_back->setWysokosc(tmp_back->getSon(0)->getWysokosc()+1);
      else{
	if(tmp_back->getSon(0)->getWysokosc()>tmp_back->getSon(1)->getWysokosc())tmp_back->setWysokosc(tmp_back->getSon(0)->getWysokosc()+1);
	else tmp_back->setWysokosc(tmp_back->getSon(1)->getWysokosc()+1);
      } //  aktualizowanie wysokosci
      // wyrownywanie drzewa
      // kazdy z czterech przypadkow jest identyczny tylko rozni sie danymi
    if(this->mod(this->wysokosc(tmp_back->getSon(0))-this->wysokosc(tmp_back->getSon(1)))>1){
      if(this->wysokosc(tmp_back->getSon(0))>this->wysokosc(tmp_back->getSon(1))){ // jezeli jest roznica wysokosci 
	if(this->wysokosc(tmp_back->getSon(0)->getSon(0))<this->wysokosc(tmp_back->getSon(0)->getSon(1))){ //  w wiekszym poddrzewie rozpatrujemy ktore poddrzewo jest wieksze
	  std::cout<<"Nastapila rotacja podwojna"<<std::endl;
	  //   3
	  //  l
	  // 1
	  //  r
	  //   2
	  std::cout<<"Zapisanie poddrzew"<<std::endl;
	  galaz1=tmp_back->getSon(0)->getSon(0);
	  galaz2=tmp_back->getSon(0)->getSon(1)->getSon(0);
	  galaz3=tmp_back->getSon(0)->getSon(1)->getSon(1);
	  galaz4=tmp_back->getSon(1);
	  std::cout<<"Rotacje"<<std::endl;	  
	  pWartosc=tmp_back->getSon(0)->getSon(1)->getZawartosc();
	  tmp_back->getSon(0)->getSon(1)->setZawartosc(tmp_back->getZawartosc());
	  tmp_back->getZawartosc()=pWartosc;
	  tmp_back->getSon(1)=tmp_back->getSon(0)->getSon(1);
	  tmp_back->getSon(0)->getParent()=tmp_back;
	  tmp_back->getSon(1)->getParent()=tmp_back;
	  tmp_back->getSon(0)->getSon(0)=galaz1;	    
	  tmp_back->getSon(0)->getSon(1)=galaz2;
	  tmp_back->getSon(1)->getSon(0)=galaz3;
	  tmp_back->getSon(1)->getSon(1)=galaz4;	    
	  std::cout<<"Przypisanie poddrzew"<<std::endl;
	  if(galaz1!=NULL)galaz1->setParent(tmp_back->getSon(0));	    
	  if(galaz2!=NULL)galaz2->setParent(tmp_back->getSon(0));
	  if(galaz3!=NULL)galaz3->setParent(tmp_back->getSon(1));
	  if(galaz4!=NULL)galaz4->setParent(tmp_back->getSon(1));	    
	  std::cout<<"Sprawdzenie wysokosci"<<std::endl;
	  if(this->wysokosc(tmp_back->getSon(1)->getSon(1))>this->wysokosc(tmp_back->getSon(1)->getSon(0)))tmp_back->getSon(1)->setWysokosc(this->wysokosc(tmp_back->getSon(1)->getSon(1))+1);
	  else tmp_back->getSon(1)->setWysokosc(this->wysokosc(tmp_back->getSon(1)->getSon(0))+1);
	  if(this->wysokosc(tmp_back->getSon(0)->getSon(1))>this->wysokosc(tmp_back->getSon(0)->getSon(0)))tmp_back->getSon(0)->setWysokosc(this->wysokosc(tmp_back->getSon(0)->getSon(1))+1);
	  else tmp_back->getSon(0)->setWysokosc(this->wysokosc(tmp_back->getSon(0)->getSon(0))+1);
	  if(tmp_back->getSon(1)->getWysokosc()>tmp_back->getSon(0)->getWysokosc())tmp_back->setWysokosc(tmp_back->getSon(1)->getWysokosc()+1);
	  else tmp_back->setWysokosc(tmp_back->getSon(0)->getWysokosc()+1);
	}else{
	  std::cout<<"Nastapila rotacja pojedyncza"<<std::endl;
	  //     3
	  //    l
	  //   2
	  //  l
	  // 1
	  std::cout<<"Zapisanie poddrzew"<<std::endl;
	  galaz1=tmp_back->getSon(0)->getSon(0)->getSon(0);
	  galaz2=tmp_back->getSon(0)->getSon(0)->getSon(1);
	  galaz3=tmp_back->getSon(0)->getSon(1);
	  galaz4=tmp_back->getSon(1);
	  std::cout<<"Rotacja"<<std::endl;	  
	  pWartosc=tmp_back->getSon(0)->getZawartosc();
	  tmp_back->getSon(0)->setZawartosc(tmp_back->getZawartosc());
	  tmp_back->getZawartosc()=pWartosc;
	  tmp_back->getSon(1)=tmp_back->getSon(0);
	  tmp_back->getSon(0)=tmp_back->getSon(1)->getSon(0);
	  tmp_back->getSon(0)->getParent()=tmp_back;
	  tmp_back->getSon(1)->getParent()=tmp_back;
	  tmp_back->getSon(0)->getSon(0)=galaz1;	    
	  tmp_back->getSon(0)->getSon(1)=galaz2;
	  tmp_back->getSon(1)->getSon(0)=galaz3;
	  tmp_back->getSon(1)->getSon(1)=galaz4;	    
	  std::cout<<"Przypisanie poddrzew"<<std::endl;
	  if(galaz1!=NULL)galaz1->setParent(tmp_back->getSon(0));	    
	  if(galaz2!=NULL)galaz2->setParent(tmp_back->getSon(0));
	  if(galaz3!=NULL)galaz3->setParent(tmp_back->getSon(1));
	  if(galaz4!=NULL)galaz4->setParent(tmp_back->getSon(1));	    
	  std::cout<<"Sprawdzenie wysokosci"<<std::endl;
	  if(this->wysokosc(tmp_back->getSon(1)->getSon(1))>this->wysokosc(tmp_back->getSon(1)->getSon(0)))tmp_back->getSon(1)->setWysokosc(this->wysokosc(tmp_back->getSon(1)->getSon(1))+1);
	  else tmp_back->getSon(1)->setWysokosc(this->wysokosc(tmp_back->getSon(1)->getSon(0))+1);
	  if(this->wysokosc(tmp_back->getSon(0)->getSon(1))>this->wysokosc(tmp_back->getSon(0)->getSon(0)))tmp_back->getSon(0)->setWysokosc(this->wysokosc(tmp_back->getSon(0)->getSon(1))+1);
	  else tmp_back->getSon(0)->setWysokosc(this->wysokosc(tmp_back->getSon(0)->getSon(0))+1);
	  if(tmp_back->getSon(1)->getWysokosc()>tmp_back->getSon(0)->getWysokosc())tmp_back->setWysokosc(tmp_back->getSon(1)->getWysokosc()+1);
	  else tmp_back->setWysokosc(tmp_back->getSon(0)->getWysokosc()+1);
	}
      }else{
	if(this->wysokosc(tmp_back->getSon(1)->getSon(0))>this->wysokosc(tmp_back->getSon(1)->getSon(1))){
	  std::cout<<"Nastapila rotacja podwojna"<<std::endl;
	  // 1
	  //  r
	  //   3
	  //  l
	  // 2
	  std::cout<<"Zapisanie poddrzew"<<std::endl;
	  galaz1=tmp_back->getSon(0);
	  galaz2=tmp_back->getSon(1)->getSon(0)->getSon(0);
	  galaz3=tmp_back->getSon(1)->getSon(0)->getSon(1);
	  galaz4=tmp_back->getSon(1)->getSon(1);
	  std::cout<<"Rotacje"<<std::endl;	  
	  pWartosc=tmp_back->getSon(1)->getSon(0)->getZawartosc();
	  tmp_back->getSon(1)->getSon(0)->setZawartosc(tmp_back->getZawartosc());
	  tmp_back->getZawartosc()=pWartosc;
	  tmp_back->getSon(0)=tmp_back->getSon(1)->getSon(0);
	  tmp_back->getSon(0)->getParent()=tmp_back;
	  tmp_back->getSon(1)->getParent()=tmp_back;
	  tmp_back->getSon(0)->getSon(0)=galaz1;	    
	  tmp_back->getSon(0)->getSon(1)=galaz2;
	  tmp_back->getSon(1)->getSon(0)=galaz3;
	  tmp_back->getSon(1)->getSon(1)=galaz4;
	  std::cout<<"Przypisanie poddrzew"<<std::endl;
	  if(galaz1!=NULL)galaz1->setParent(tmp_back->getSon(0));	    
	  if(galaz2!=NULL)galaz2->setParent(tmp_back->getSon(0));
	  if(galaz3!=NULL)galaz3->setParent(tmp_back->getSon(1));
	  if(galaz4!=NULL)galaz4->setParent(tmp_back->getSon(1));	    
	  std::cout<<"Sprawdzenie wysokosci"<<std::endl;
	  if(this->wysokosc(tmp_back->getSon(1)->getSon(1))>this->wysokosc(tmp_back->getSon(1)->getSon(0)))tmp_back->getSon(1)->setWysokosc(this->wysokosc(tmp_back->getSon(1)->getSon(1))+1);
	  else tmp_back->getSon(1)->setWysokosc(this->wysokosc(tmp_back->getSon(1)->getSon(0))+1);
	  if(this->wysokosc(tmp_back->getSon(0)->getSon(1))>this->wysokosc(tmp_back->getSon(0)->getSon(0)))tmp_back->getSon(0)->setWysokosc(this->wysokosc(tmp_back->getSon(0)->getSon(1))+1);
	  else tmp_back->getSon(0)->setWysokosc(this->wysokosc(tmp_back->getSon(0)->getSon(0))+1);
	  if(tmp_back->getSon(1)->getWysokosc()>tmp_back->getSon(0)->getWysokosc())tmp_back->setWysokosc(tmp_back->getSon(1)->getWysokosc()+1);
	  else tmp_back->setWysokosc(tmp_back->getSon(0)->getWysokosc()+1);
	}else{
	  std::cout<<"Nastapila rotacja pojedyncza"<<std::endl;
	  // 1
	  //  r
	  //   2
	  //    r
	  //     3
	  std::cout<<"Zapisanie poddrzew"<<std::endl;
	  galaz1=tmp_back->getSon(0);
	  galaz2=tmp_back->getSon(1)->getSon(0);
	  galaz3=tmp_back->getSon(1)->getSon(1)->getSon(0);
	  galaz4=tmp_back->getSon(1)->getSon(1)->getSon(1);
	  std::cout<<"Rotacja"<<std::endl;	  
	  pWartosc=tmp_back->getSon(1)->getZawartosc();
	  tmp_back->getSon(1)->setZawartosc(tmp_back->getZawartosc());
	  tmp_back->getZawartosc()=pWartosc;
	  tmp_back->getSon(0)=tmp_back->getSon(1);
	  tmp_back->getSon(1)=tmp_back->getSon(0)->getSon(1);
	  tmp_back->getSon(0)->getParent()=tmp_back;
	  tmp_back->getSon(1)->getParent()=tmp_back;
	  tmp_back->getSon(0)->getSon(0)=galaz1;	    
	  tmp_back->getSon(0)->getSon(1)=galaz2;
	  tmp_back->getSon(1)->getSon(0)=galaz3;
	  tmp_back->getSon(1)->getSon(1)=galaz4;
	  std::cout<<"Przypisanie poddrzew"<<std::endl;
	  if(galaz1!=NULL)galaz1->setParent(tmp_back->getSon(0));	    
	  if(galaz2!=NULL)galaz2->setParent(tmp_back->getSon(0));
	  if(galaz3!=NULL)galaz3->setParent(tmp_back->getSon(1));
	  if(galaz4!=NULL)galaz4->setParent(tmp_back->getSon(1));	    
	  std::cout<<"Sprawdzenie wysokosci"<<std::endl;
	  if(this->wysokosc(tmp_back->getSon(1)->getSon(1))>this->wysokosc(tmp_back->getSon(1)->getSon(0)))tmp_back->getSon(1)->setWysokosc(this->wysokosc(tmp_back->getSon(1)->getSon(1))+1);
	  else tmp_back->getSon(1)->setWysokosc(this->wysokosc(tmp_back->getSon(1)->getSon(0))+1);
	  if(this->wysokosc(tmp_back->getSon(0)->getSon(1))>this->wysokosc(tmp_back->getSon(0)->getSon(0)))tmp_back->getSon(0)->setWysokosc(this->wysokosc(tmp_back->getSon(0)->getSon(1))+1);
	  else tmp_back->getSon(0)->setWysokosc(this->wysokosc(tmp_back->getSon(0)->getSon(0))+1);
	  if(tmp_back->getSon(1)->getWysokosc()>tmp_back->getSon(0)->getWysokosc())tmp_back->setWysokosc(tmp_back->getSon(1)->getWysokosc()+1);
	  else tmp_back->setWysokosc(tmp_back->getSon(0)->getWysokosc()+1);
	}
      }
    }
    tmp_back=tmp_back->getParent();
    }
    if(dl)return zawartosc;
  }
  throw TreeEmptyException();
}

template <class Object>                   // drzewo binarne ktore dziedzicy po ogolnym
class BinaryTree:public Tree<Object>{
public:
  Object& dodaj(const Object& obj);
};

template <class Object>  // dodaje element wiekszy lub rowny na prawej galezi a mniejszy na lewej. mechanizmy podobne jak w funkcji zaslonietej
Object& BinaryTree<Object>::dodaj(const Object& obj){
  wezelDrzewa<Object> *wezel=NULL;
  wezelDrzewa<Object> *tmp_back=NULL;
  wezelDrzewa<Object> *tmp=this->root;
  wezelDrzewa<Object> *galaz1=NULL;
  wezelDrzewa<Object> *galaz2=NULL;
  wezelDrzewa<Object> *galaz3=NULL;
  wezelDrzewa<Object> *galaz4=NULL;
  Object pWartosc;
  wezel=new wezelDrzewa<Object>;
  wezel->setZawartosc(obj);
  this->zawartosc=wezel->getZawartosc();
  (this->dl)++;
  while(tmp!=NULL){
    tmp_back=tmp;
    if((wezel->getZawartosc())<(tmp->getZawartosc())){
      tmp=tmp->getSon(0);
    }else{
      tmp=tmp->getSon(1);
    }
  }
  wezel->setParent(tmp_back);
  if(tmp_back==NULL){
    this->root=wezel;
  }else{
    if(wezel->getZawartosc()<tmp_back->getZawartosc()){
      tmp_back->getSon(0)=wezel;
    }else{
      tmp_back->getSon(1)=wezel;
    }
  }
  while(tmp_back!=NULL){
    if((tmp_back->getSon(0)==NULL)&&(tmp_back->getSon(1)==NULL))tmp_back->setWysokosc(1);
    else if(tmp_back->getSon(0)==NULL)tmp_back->setWysokosc(tmp_back->getSon(1)->getWysokosc()+1);
    else if(tmp_back->getSon(1)==NULL)tmp_back->setWysokosc(tmp_back->getSon(0)->getWysokosc()+1);
    else{
      if(tmp_back->getSon(0)->getWysokosc()>tmp_back->getSon(1)->getWysokosc())tmp_back->setWysokosc(tmp_back->getSon(0)->getWysokosc()+1);
      else tmp_back->setWysokosc(tmp_back->getSon(1)->getWysokosc()+1);
    }
    // wyrownywanie drzewa analogicznie jak w funcji usuwania
    if(this->mod(this->wysokosc(tmp_back->getSon(0))-this->wysokosc(tmp_back->getSon(1)))>1){
      if(this->wysokosc(tmp_back->getSon(0))>this->wysokosc(tmp_back->getSon(1))){
	if(this->wysokosc(tmp_back->getSon(0)->getSon(0))<this->wysokosc(tmp_back->getSon(0)->getSon(1))){
	  std::cout<<"Nastapila rotacja podwojna"<<std::endl;
	  //   3
	  //  l
	  // 1
	  //  r
	  //   2
	  std::cout<<"Zapisanie poddrzew"<<std::endl;
	  galaz1=tmp_back->getSon(0)->getSon(0);
	  galaz2=tmp_back->getSon(0)->getSon(1)->getSon(0);
	  galaz3=tmp_back->getSon(0)->getSon(1)->getSon(1);
	  galaz4=tmp_back->getSon(1);
	  std::cout<<"Rotacje"<<std::endl;	  
	  pWartosc=tmp_back->getSon(0)->getSon(1)->getZawartosc();
	  tmp_back->getSon(0)->getSon(1)->setZawartosc(tmp_back->getZawartosc());
	  tmp_back->getZawartosc()=pWartosc;
	  tmp_back->getSon(1)=tmp_back->getSon(0)->getSon(1);
	  tmp_back->getSon(0)->getParent()=tmp_back;
	  tmp_back->getSon(1)->getParent()=tmp_back;
	  tmp_back->getSon(0)->getSon(0)=galaz1;	    
	  tmp_back->getSon(0)->getSon(1)=galaz2;
	  tmp_back->getSon(1)->getSon(0)=galaz3;
	  tmp_back->getSon(1)->getSon(1)=galaz4;	    
	  std::cout<<"Przypisanie poddrzew"<<std::endl;
	  if(galaz1!=NULL)galaz1->setParent(tmp_back->getSon(0));	    
	  if(galaz2!=NULL)galaz2->setParent(tmp_back->getSon(0));
	  if(galaz3!=NULL)galaz3->setParent(tmp_back->getSon(1));
	  if(galaz4!=NULL)galaz4->setParent(tmp_back->getSon(1));	    
	  std::cout<<"Sprawdzenie wysokosci"<<std::endl;
	  if(this->wysokosc(tmp_back->getSon(1)->getSon(1))>this->wysokosc(tmp_back->getSon(1)->getSon(0)))tmp_back->getSon(1)->setWysokosc(this->wysokosc(tmp_back->getSon(1)->getSon(1))+1);
	  else tmp_back->getSon(1)->setWysokosc(this->wysokosc(tmp_back->getSon(1)->getSon(0))+1);
	  if(this->wysokosc(tmp_back->getSon(0)->getSon(1))>this->wysokosc(tmp_back->getSon(0)->getSon(0)))tmp_back->getSon(0)->setWysokosc(this->wysokosc(tmp_back->getSon(0)->getSon(1))+1);
	  else tmp_back->getSon(0)->setWysokosc(this->wysokosc(tmp_back->getSon(0)->getSon(0))+1);
	  if(tmp_back->getSon(1)->getWysokosc()>tmp_back->getSon(0)->getWysokosc())tmp_back->setWysokosc(tmp_back->getSon(1)->getWysokosc()+1);
	  else tmp_back->setWysokosc(tmp_back->getSon(0)->getWysokosc()+1);
	}else{
	  std::cout<<"Nastapila rotacja pojedyncza"<<std::endl;
	  //     3
	  //    l
	  //   2
	  //  l
	  // 1
	  std::cout<<"Zapisanie poddrzew"<<std::endl;
	  galaz1=tmp_back->getSon(0)->getSon(0)->getSon(0);
	  galaz2=tmp_back->getSon(0)->getSon(0)->getSon(1);
	  galaz3=tmp_back->getSon(0)->getSon(1);
	  galaz4=tmp_back->getSon(1);
	  std::cout<<"Rotacja"<<std::endl;	  
	  pWartosc=tmp_back->getSon(0)->getZawartosc();
	  tmp_back->getSon(0)->setZawartosc(tmp_back->getZawartosc());
	  tmp_back->getZawartosc()=pWartosc;
	  tmp_back->getSon(1)=tmp_back->getSon(0);
	  tmp_back->getSon(0)=tmp_back->getSon(1)->getSon(0);
	  tmp_back->getSon(0)->getParent()=tmp_back;
	  tmp_back->getSon(1)->getParent()=tmp_back;
	  tmp_back->getSon(0)->getSon(0)=galaz1;	    
	  tmp_back->getSon(0)->getSon(1)=galaz2;
	  tmp_back->getSon(1)->getSon(0)=galaz3;
	  tmp_back->getSon(1)->getSon(1)=galaz4;	    
	  std::cout<<"Przypisanie poddrzew"<<std::endl;
	  if(galaz1!=NULL)galaz1->setParent(tmp_back->getSon(0));	    
	  if(galaz2!=NULL)galaz2->setParent(tmp_back->getSon(0));
	  if(galaz3!=NULL)galaz3->setParent(tmp_back->getSon(1));
	  if(galaz4!=NULL)galaz4->setParent(tmp_back->getSon(1));	    
	  std::cout<<"Sprawdzenie wysokosci"<<std::endl;
	  if(this->wysokosc(tmp_back->getSon(1)->getSon(1))>this->wysokosc(tmp_back->getSon(1)->getSon(0)))tmp_back->getSon(1)->setWysokosc(this->wysokosc(tmp_back->getSon(1)->getSon(1))+1);
	  else tmp_back->getSon(1)->setWysokosc(this->wysokosc(tmp_back->getSon(1)->getSon(0))+1);
	  if(this->wysokosc(tmp_back->getSon(0)->getSon(1))>this->wysokosc(tmp_back->getSon(0)->getSon(0)))tmp_back->getSon(0)->setWysokosc(this->wysokosc(tmp_back->getSon(0)->getSon(1))+1);
	  else tmp_back->getSon(0)->setWysokosc(this->wysokosc(tmp_back->getSon(0)->getSon(0))+1);
	  if(tmp_back->getSon(1)->getWysokosc()>tmp_back->getSon(0)->getWysokosc())tmp_back->setWysokosc(tmp_back->getSon(1)->getWysokosc()+1);
	  else tmp_back->setWysokosc(tmp_back->getSon(0)->getWysokosc()+1);
	}
      }else{
	if(this->wysokosc(tmp_back->getSon(1)->getSon(0))>this->wysokosc(tmp_back->getSon(1)->getSon(1))){
	  std::cout<<"Nastapila rotacja podwojna"<<std::endl;
	  // 1
	  //  r
	  //   3
	  //  l
	  // 2
	  std::cout<<"Zapisanie poddrzew"<<std::endl;
	  galaz1=tmp_back->getSon(0);
	  galaz2=tmp_back->getSon(1)->getSon(0)->getSon(0);
	  galaz3=tmp_back->getSon(1)->getSon(0)->getSon(1);
	  galaz4=tmp_back->getSon(1)->getSon(1);
	  std::cout<<"Rotacje"<<std::endl;	  
	  pWartosc=tmp_back->getSon(1)->getSon(0)->getZawartosc();
	  tmp_back->getSon(1)->getSon(0)->setZawartosc(tmp_back->getZawartosc());
	  tmp_back->getZawartosc()=pWartosc;
	  tmp_back->getSon(0)=tmp_back->getSon(1)->getSon(0);
	  tmp_back->getSon(0)->getParent()=tmp_back;
	  tmp_back->getSon(1)->getParent()=tmp_back;
	  tmp_back->getSon(0)->getSon(0)=galaz1;	    
	  tmp_back->getSon(0)->getSon(1)=galaz2;
	  tmp_back->getSon(1)->getSon(0)=galaz3;
	  tmp_back->getSon(1)->getSon(1)=galaz4;
	  std::cout<<"Przypisanie poddrzew"<<std::endl;
	  if(galaz1!=NULL)galaz1->setParent(tmp_back->getSon(0));	    
	  if(galaz2!=NULL)galaz2->setParent(tmp_back->getSon(0));
	  if(galaz3!=NULL)galaz3->setParent(tmp_back->getSon(1));
	  if(galaz4!=NULL)galaz4->setParent(tmp_back->getSon(1));	    
	  std::cout<<"Sprawdzenie wysokosci"<<std::endl;
	  if(this->wysokosc(tmp_back->getSon(1)->getSon(1))>this->wysokosc(tmp_back->getSon(1)->getSon(0)))tmp_back->getSon(1)->setWysokosc(this->wysokosc(tmp_back->getSon(1)->getSon(1))+1);
	  else tmp_back->getSon(1)->setWysokosc(this->wysokosc(tmp_back->getSon(1)->getSon(0))+1);
	  if(this->wysokosc(tmp_back->getSon(0)->getSon(1))>this->wysokosc(tmp_back->getSon(0)->getSon(0)))tmp_back->getSon(0)->setWysokosc(this->wysokosc(tmp_back->getSon(0)->getSon(1))+1);
	  else tmp_back->getSon(0)->setWysokosc(this->wysokosc(tmp_back->getSon(0)->getSon(0))+1);
	  if(tmp_back->getSon(1)->getWysokosc()>tmp_back->getSon(0)->getWysokosc())tmp_back->setWysokosc(tmp_back->getSon(1)->getWysokosc()+1);
	  else tmp_back->setWysokosc(tmp_back->getSon(0)->getWysokosc()+1);
	}else{
	  std::cout<<"Nastapila rotacja pojedyncza"<<std::endl;
	  // 1
	  //  r
	  //   2
	  //    r
	  //     3
	  std::cout<<"Zapisanie poddrzew"<<std::endl;
	  galaz1=tmp_back->getSon(0);
	  galaz2=tmp_back->getSon(1)->getSon(0);
	  galaz3=tmp_back->getSon(1)->getSon(1)->getSon(0);
	  galaz4=tmp_back->getSon(1)->getSon(1)->getSon(1);
	  std::cout<<"Rotacja"<<std::endl;	  
	  pWartosc=tmp_back->getSon(1)->getZawartosc();
	  tmp_back->getSon(1)->setZawartosc(tmp_back->getZawartosc());
	  tmp_back->getZawartosc()=pWartosc;
	  tmp_back->getSon(0)=tmp_back->getSon(1);
	  tmp_back->getSon(1)=tmp_back->getSon(0)->getSon(1);
	  tmp_back->getSon(0)->getParent()=tmp_back;
	  tmp_back->getSon(1)->getParent()=tmp_back;
	  tmp_back->getSon(0)->getSon(0)=galaz1;	    
	  tmp_back->getSon(0)->getSon(1)=galaz2;
	  tmp_back->getSon(1)->getSon(0)=galaz3;
	  tmp_back->getSon(1)->getSon(1)=galaz4;
	  std::cout<<"Przypisanie poddrzew"<<std::endl;
	  if(galaz1!=NULL)galaz1->setParent(tmp_back->getSon(0));	    
	  if(galaz2!=NULL)galaz2->setParent(tmp_back->getSon(0));
	  if(galaz3!=NULL)galaz3->setParent(tmp_back->getSon(1));
	  if(galaz4!=NULL)galaz4->setParent(tmp_back->getSon(1));	    
	  std::cout<<"Sprawdzenie wysokosci"<<std::endl;
	  if(this->wysokosc(tmp_back->getSon(1)->getSon(1))>this->wysokosc(tmp_back->getSon(1)->getSon(0)))tmp_back->getSon(1)->setWysokosc(this->wysokosc(tmp_back->getSon(1)->getSon(1))+1);
	  else tmp_back->getSon(1)->setWysokosc(this->wysokosc(tmp_back->getSon(1)->getSon(0))+1);
	  if(this->wysokosc(tmp_back->getSon(0)->getSon(1))>this->wysokosc(tmp_back->getSon(0)->getSon(0)))tmp_back->getSon(0)->setWysokosc(this->wysokosc(tmp_back->getSon(0)->getSon(1))+1);
	  else tmp_back->getSon(0)->setWysokosc(this->wysokosc(tmp_back->getSon(0)->getSon(0))+1);
	  if(tmp_back->getSon(1)->getWysokosc()>tmp_back->getSon(0)->getWysokosc())tmp_back->setWysokosc(tmp_back->getSon(1)->getWysokosc()+1);
	  else tmp_back->setWysokosc(tmp_back->getSon(0)->getWysokosc()+1);
	}
      }
    }
    tmp_back=tmp_back->getParent();
  }
  return this->zawartosc;
}
