class matrixEmptyException{}; //  zwracanie bledu

class macierzSasiedztwa{
protected:
  doubleList<doubleList<bool> > dane; //  lista list czyli macierz
  doubleList<bool> tmp;
public:
  int rozmiar(){return dane.rozmiar();} //  jeden wymiar macierzy
  macierzSasiedztwa(int a){ //  inicjalizacja macierzy sasiedzctwa o wielkosci a x a
    for(int i=0;i<a;i++){
      dane.wstawOstatni(tmp);
    }
    for(int i=0;i<dane.rozmiar();i++){
      for(int j=0;j<dane.rozmiar();j++){
	dane[i].wstawOstatni(false);
      }
    }
  }
  ~macierzSasiedztwa(){ //  zwalnianie pamieci zajmowaniej przez macierz
    for(int i=0;i<dane.rozmiar();i++){
      dane[i].clear();
    }
    dane.clear();
  }
  void screen(){ //  wyswietlanie macierzy
    for(int i=0;i<dane.rozmiar();i++){
      dane[i].screen(); //  wyswietlanie podlist listy
      std::cout<<std::endl;
    }
  }
  void clear(){ //  czyszczenie macierzy czyli przypisywanie false wszystkim komorka
    for(int i=0;i<dane.rozmiar();i++){
      for(int j=0;j<dane.rozmiar();j++){
	dane[i][j]=false;
      }
    } 
  }
  doubleList<bool>& operator[](int i) throw(matrixEmptyException){ //  zwraca dostemp do wskazanej podlisty listy
    try{
      return dane[i];
    }catch(doubleListEmptyException){
      throw matrixEmptyException();
    }
  }
};

