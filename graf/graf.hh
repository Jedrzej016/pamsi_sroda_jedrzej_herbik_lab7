#include "doubleList.hh"
 
class grafEmptyException{}; //  wyrzucanie bledu
class doNotTouch{}; //  wyrzucanie bledu

template <class Object,class Connect>
class edge; //  inicjalizacja krawedzi

template <class Object,class Connect> //  wierzcholki
class vertex{
private:
  Object wartosc;
  doubleList<edge<Object,Connect>* > edges; //  lista wskazujacych na ten wierzcholek krawedzi
public:
  ~vertex(){edges.clear();}
  edge<Object,Connect>*& operator[](int i){  // zwracanie wskazanej krawedzie z listy
    if(i>=edges.rozmiar())throw doNotTouch(); //  wskazanie po za liste
    return edges[i];
  }
  Object& getZawartosc(){return wartosc;}
  doubleList<edge<Object,Connect>* >& getList(){return edges;} //  zwraca dostep do listy
  bool operator!=(vertex<Object,Connect>& a){if(this!=&a)return true;return false;}
  bool operator==(vertex<Object,Connect>& a){if(wartosc==a.getZawartosc())return true;return false;}
};

template <class Object,class Connect> //  wyswietlanie wierzcholkow
std::ostream& operator << (std::ostream& StrmWy,vertex<Object,Connect> &WyswietlanaWartosc){
  std::cout<<WyswietlanaWartosc.getZawartosc()<<" ";
  return StrmWy;  
}

template <class Object,class Connect> //  krawedzie
class edge{
private:
  Connect wartosc;
  bool wyswietl; //  czy wartosc krawedzi ma byc wyswietlona
  bool exist;
  vertex<Object,Connect>* v1; //  pierwszy wskazywany element
  vertex<Object,Connect>* v2; //  drugi wskazywany element
public:
  edge(){wyswietl=false;exist=false;}
  bool& getWyswietl(){return wyswietl;}
  bool& getExist(){return exist;}
  Connect& getZawartosc(){return wartosc;}
  vertex<Object,Connect>*& getV1(){return v1;}
  vertex<Object,Connect>*& getV2(){return v2;}
  bool operator!=(edge<Object,Connect>& a){if(this!=&a)return true;return false;}
  bool operator==(edge<Object,Connect>& a){if(this==&a)return true;return false;}
};

template <class Object,class Connect> //  wyswietlanie krawedzi
std::ostream& operator << (std::ostream& StrmWy,edge<Object,Connect> &WyswietlanaWartosc){
  if(WyswietlanaWartosc.getWyswietl())std::cout<<std::endl<<*WyswietlanaWartosc.getV1()<<" >> "<<WyswietlanaWartosc.getZawartosc()<<" >> "<<*WyswietlanaWartosc.getV2();
  else std::cout<<std::endl<<*WyswietlanaWartosc.getV1()<<" >>>> "<<*WyswietlanaWartosc.getV2();
  return StrmWy;  
}

template <class Object,class Connect> //  klasa grafu
class graf{
protected:
  doubleList<vertex<Object,Connect> >V; //  lista wierzcholkow
  doubleList<edge<Object,Connect> >E; //  lista krawedzi
  int dlMatrix;
  edge<Object,Connect> **matrix;
  int typGrafu;
  int findint(vertex<Object,Connect>& v);
  void clear(edge<Object,Connect> **tablica,int dl);
  void przepisz(edge<Object,Connect> **newtablica,edge<Object,Connect> **tablica,int dl);
public:
  graf(){typGrafu=0;dlMatrix=0;}
  graf(int a){typGrafu=a;dlMatrix=0;}
  ~graf(){if(typGrafu){}else{V.clear();E.clear();}}
  void clear(); //  czyszczenie listy krawedzi i listy wierzcholkow
  vertex<Object,Connect>& findVertex(Object dane)throw(grafEmptyException); //  znajdowanie adresu wierzcholka na podstawie zawartosci
  edge<Object,Connect>& findEdge(Connect dane)throw(grafEmptyException); //  znajdowanie adresu krawedzi na podstawie zawartosci
  Object& insertVertex(Object dane); //  dodawanie wierzcholka
  Object& removeVertex(vertex<Object,Connect>& a)throw(grafEmptyException); //  usuwanie wierzcholka
  void insertEdge(vertex<Object,Connect>& a,vertex<Object,Connect>& b);  // dodanie krawedzi
  Connect insertEdge(vertex<Object,Connect>& a,vertex<Object,Connect>& b,Connect dana); // dodanie krawedzi
  Connect removeEdge(edge<Object,Connect>& a)throw(grafEmptyException);  // usuwanie krawedzi
  int edges(); //  wyswietlenie polaczen
  int vertices(); //  wyswietlenie elementow
  int incidentEdges(vertex<Object,Connect>& v); //  wyswietlenie krawedzi przyleglych do wskazanego wierzcholka
  vertex<Object,Connect>& endVerticies(edge<Object,Connect>& e,int i)throw(grafEmptyException);  //wierzcholki na koncach wskazanej krawedzi
  vertex<Object,Connect>& opposite(vertex<Object,Connect>& v,edge<Object,Connect>& e)throw(grafEmptyException); //  wierzcholek po przeciwnej stronie wzgledem wskazanej krawedzi i wierzcholka
  bool areAdjacent(vertex<Object,Connect>& v1,vertex<Object,Connect>& v2); //  czy sa polaczone
  void replace(vertex<Object,Connect>& v,Object x); //  zamien zawartosc wskazanego wierzcholka
  void replace(edge<Object,Connect>& e,Connect x); //  zamien zawartosc wskazanej krawedzi
  vertex<Object,Connect>& operator[](int i){return V[i];} //  zwroc wskazany element z listy wierzcholkow
  edge<Object,Connect>& operator()(int i){return E[i];} //  zwroc wskazany element z listy krawedzi
  edge<Object,Connect>& operator()(int i,int j){return matrix[i][j];} //  zwroc wskazany element z listy krawedzi
};

template <class Object,class Connect>
void graf<Object,Connect>::clear(edge<Object,Connect> **tablica,int dl){
  for(int i=0;i<dl;i++){
    delete [] tablica[i];
  }
  delete [] tablica; // zwalnianie pamieci z tablicy wskaznikow odpowiadajacej kolumna
}

template <class Object,class Connect>
void graf<Object,Connect>::przepisz(edge<Object,Connect> **newtablica,edge<Object,Connect> **tablica,int dl){
  for(int i=0;i<dl;i++){
    for(int j=0;j<dl;j++){
      newtablica[i][j]=tablica[i][j];
    }
  }
  clear(tablica,dl);
  tablica=newtablica;
}

template<class Object,class Connect>
void graf<Object,Connect>::clear(){
  if(typGrafu){
    clear(matrix,dlMatrix);
  }else{
    E.clear(); //  czyszczenie listy krawedzi
    V.clear(); //  czyszczenie listy wierzcholkow
  }
}

template<class Object,class Connect>
int graf<Object,Connect>::findint(vertex<Object,Connect>& v){
  int i=0;
  while(1){
    if(v==V[i])return i;
    i++;
    if(i==V.rozmiar())return -1;
  }
}

template<class Object,class Connect>
vertex<Object,Connect>& graf<Object,Connect>::findVertex(Object dane)throw(grafEmptyException){
  for(int i=0;i<V.rozmiar();i++){
    if(V[i].getZawartosc()==dane)return V[i]; //  zwraca wierzcholek o wskazanej wartosci
    }
  throw grafEmptyException(); //  jezeli nie ma takiego wierzcholku to zwroc blad
}

template<class Object,class Connect>
edge<Object,Connect>& graf<Object,Connect>::findEdge(Connect dane)throw(grafEmptyException){
  if(typGrafu){
    for(int i=0;i<dlMatrix;i++){
      for(int j=0;j<dlMatrix;j++){
	if(matrix[i][j].getZawartosc()==dane)return matrix[i][j]; //  zwraca krawedz o wskazanej wartosci
      }
    }
    throw grafEmptyException(); //  jeżeli nie ma takiej krawedzi o wskazanej wartosci
  }else{
    for(int i=0;i<E.rozmiar();i++){
      if(E[i].getZawartosc()==dane)return E[i]; //  zwraca krawedz o wskazanej wartosci
    }
    throw grafEmptyException(); //  jeżeli nie ma takiej krawedzi o wskazanej wartosci
  }
}

template<class Object,class Connect>
Object& graf<Object,Connect>::insertVertex(Object dane){
  vertex<Object,Connect> tmp;
  edge<Object,Connect> **tablica;  
  if(typGrafu){
    dlMatrix++;
    tablica=new edge<Object,Connect>* [dlMatrix]; // stworzenie tablicy wskaznikow o dlugosci wiersza tablicy
    for(int i=0;i<dlMatrix;i++){
      tablica[i]=new edge<Object,Connect> [dlMatrix]; // tworzenie kolumn z tablicy wskaznikow
    }
    if(dlMatrix>1){
      przepisz(tablica,matrix,dlMatrix-1);
      matrix=tablica;
    }else matrix=tablica;
  }
  tmp.getZawartosc()=dane;
  V.wstawOstatni(tmp); //  dodanie elementu do listy
  return tmp.getZawartosc();
}

template<class Object,class Connect>
Object& graf<Object,Connect>::removeVertex(vertex<Object,Connect>& a)throw(grafEmptyException){
  edge<Object,Connect> **newtablica;
  int wiersz;
  if(typGrafu){
    wiersz=findint(a);
    newtablica=new edge<Object,Connect>* [dlMatrix-1]; // stworzenie tablicy wskaznikow o dlugosci wiersza tablicy
    for(int i=0;i<(dlMatrix-1);i++){
      newtablica[i]=new edge<Object,Connect> [dlMatrix-1]; // tworzenie kolumn z tablicy wskaznikow
    }
    for(int i=0;i<(wiersz);i++){
      for(int j=0;j<(wiersz);j++){
	newtablica[j][i]=matrix[j][i]; 
      } 
    }
    for(int i=wiersz;i<(dlMatrix-1);i++){
      for(int j=wiersz;j<(dlMatrix-1);j++){
	newtablica[j][i]=matrix[j+1][i+1]; 
      } 
    }
    for(int i=wiersz;i<(dlMatrix-1);i++){
      for(int j=0;j<(wiersz);j++){
	newtablica[j][i]=matrix[j][i+1]; 
      } 
    }
    for(int i=0;i<(wiersz);i++){
      for(int j=wiersz;j<(dlMatrix-1);j++){
	newtablica[j][i]=matrix[j+1][i]; 
      } 
    }
    clear(matrix,dlMatrix);
    dlMatrix--;
    matrix=newtablica;
    newtablica=NULL;
    try{
      return V.usunWskazany(a).getZawartosc(); //  zwraca zawartosc usuwanego wierzcholka
    }catch(doubleListEmptyException){
      throw grafEmptyException(); //  jezeli wskazany wierzcholek nie istenieje
    }
  }else{
    try{
      while(!a.getList().isEmpty()){E.usunWskazany(*a.getList().usunPierwszy());}
      return V.usunWskazany(a).getZawartosc(); //  zwraca zawartosc usuwanego wierzcholka
    }catch(doubleListEmptyException){
      throw grafEmptyException(); //  jezeli wskazany wierzcholek nie istenieje
    }
  }
}

template<class Object,class Connect>
void graf<Object,Connect>::insertEdge(vertex<Object,Connect>& a,vertex<Object,Connect>& b){ //  zawartosc dodanej krawedzi to 0
  edge<Object,Connect> tmp;
  if(typGrafu){
    matrix[this->findint(b)][this->findint(a)].getWyswietl()=false;
    matrix[this->findint(b)][this->findint(a)].getExist()=true;
    matrix[this->findint(b)][this->findint(a)].getV1()=&a;
    matrix[this->findint(b)][this->findint(a)].getV2()=&b;
  }else{
    tmp.getZawartosc()=0;
    tmp.getV1()=&a;
    tmp.getV2()=&b;
    E.wstawOstatni(tmp); //  dodanie krawedzi do listy
    a.getList().wstawOstatni(&E.ostatni()); //  przypisanie w wierzcholku dodanej krawedzi
    b.getList().wstawOstatni(&E.ostatni()); //  przypisanie w wierzcholku dodanej krawedzi
  }
}

template<class Object,class Connect>
Connect graf<Object,Connect>::removeEdge(edge<Object,Connect>& a)throw(grafEmptyException){
  edge<Object,Connect>* tmp=&a;
  if(typGrafu){
    for(int i=0;i<dlMatrix;i++){
      for(int j=0;j<dlMatrix;j++){
	if(matrix[i][j]==a){
	  matrix[i][j].getWyswietl()=false;
	  matrix[i][j].getExist()=false;
	  matrix[i][j].getV1()=NULL;
	  matrix[i][j].getV2()=NULL;    
	  return matrix[i][j].getZawartosc(); //  zwraca wartosc usuwanej krawedzi
	}
      }
    }
    throw grafEmptyException(); //  gdy wskazana krawedz nie istnieje    
  }else{
    try{
      a.getV1()->getList().usunWskazany(tmp);
      a.getV2()->getList().usunWskazany(tmp);
      return E.usunWskazany(a).getZawartosc(); //  zwraca wartosc usuwanej krawedzi
    }catch(doubleListEmptyException){
      throw grafEmptyException(); //  gdy wskazana krawedz nie istnieje
    }
  }
}

template<class Object,class Connect>
Connect graf<Object,Connect>::insertEdge(vertex<Object,Connect>& a,vertex<Object,Connect>& b,Connect dana){ //  dodawanie krawedzi ale z podana jej wartoscia
  edge<Object,Connect> tmp;
  if(typGrafu){
    matrix[this->findint(b)][this->findint(a)].getWyswietl()=true;
    matrix[this->findint(b)][this->findint(a)].getZawartosc()=dana;
    matrix[this->findint(b)][this->findint(a)].getExist()=true;
    matrix[this->findint(b)][this->findint(a)].getV1()=&a;
    matrix[this->findint(b)][this->findint(a)].getV2()=&b;
    return matrix[this->findint(b)][this->findint(a)].getZawartosc();
  }else{
    tmp.getZawartosc()=dana;
    tmp.getV1()=&a;
    tmp.getV2()=&b;
    tmp.getWyswietl()=true;
    E.wstawOstatni(tmp);
    a.getList().wstawOstatni(&E.ostatni());
    b.getList().wstawOstatni(&E.ostatni());
    return tmp.getZawartosc();
  }
}

template<class Object,class Connect>
int graf<Object,Connect>::edges(){
  int liczba=0;
  if(typGrafu){
    for(int j=0;j<dlMatrix;j++){
      for(int i=0;i<dlMatrix;i++){
        if(matrix[i][j].getExist()){
	  std::cout<<V[j]<<">> "<<matrix[i][j].getZawartosc()<<" >> "<<V[i]<<std::endl;
	  liczba++;
	}
      }
    }
    return liczba;
  }else{
    if(E.isEmpty())return 0; //  gdy pusta to zwraca 0
    E.screen(); //  wyswietlenie listy krawedzi
    std::cout<<std::endl; // zwraca rozmiar listy
    return E.rozmiar();
  }
}

template<class Object,class Connect>
int graf<Object,Connect>::vertices(){
  if(V.isEmpty())return 0; //  gdy pusta to zwraca 0
  V.screen(); //  wyswietlenie listy wierzcholkow
  return V.rozmiar(); // zwraca rozmiar listy
}

template<class Object,class Connect>
int graf<Object,Connect>::incidentEdges(vertex<Object,Connect>& v){
  int liczba=0;
  int wartosc=0;
  if(typGrafu){
    wartosc=this->findint(v);
    for(int i=0;i<dlMatrix;i++){
      if(matrix[i][wartosc].getExist()){
	std::cout<<std::endl<<V[wartosc]<<">> "<<matrix[i][wartosc].getZawartosc()<<" >> "<<V[i];
	liczba++;
      }
    }
    std::cout<<std::endl;
    return liczba; //  zwraca rozmiar wyswietlonej listy
  }else{
    for(int i=0;i<v.getList().rozmiar();i++){
      std::cout<<*v[i]<<" "; //  wyswietla krawedzie przylegle
    }
    std::cout<<std::endl;
    return v.getList().rozmiar(); //  zwraca rozmiar wyswietlonej listy
  }
}

template<class Object,class Connect>
vertex<Object,Connect>& graf<Object,Connect>::endVerticies(edge<Object,Connect>& e,int i)throw(grafEmptyException){
  if(typGrafu){
    if(e.getExist()){
      if(i)return *e.getV2(); //  zwraca adres jednego wierzcholka laczonego przez wskazana krawedz
      return *e.getV1(); //  zwraca adres jednego wierzcholka laczonego przez wskazana krawedz
    }else throw grafEmptyException();
  }else{
    if(i)return *e.getV2(); //  zwraca adres jednego wierzcholka laczonego przez wskazana krawedz
    return *e.getV1(); //  zwraca adres jednego wierzcholka laczonego przez wskazana krawedz
  }
}

template<class Object,class Connect>
vertex<Object,Connect>& graf<Object,Connect>::opposite(vertex<Object,Connect>& v,edge<Object,Connect>& e)throw(grafEmptyException){
  if(e.getV1()==&v)return *e.getV2(); //  jezeli V1 to jest podany wierzcholek to zwroc V2
  if(e.getV2()==&v)return *e.getV1(); //  jezeli V1 to jest podany wierzcholek to zwroc V2
  throw grafEmptyException();
}

template<class Object,class Connect>
bool graf<Object,Connect>::areAdjacent(vertex<Object,Connect>& v1,vertex<Object,Connect>& v2){
  if(typGrafu){
    return matrix[findint(v2)][findint(v1)].getExist();
  }else{
    for(int i=0;i<v1.getList().rozmiar();i++){
      //      if((v1[i]->getV1()==&v2)||(v1[i]->getV2()==&v2))return true; //  zwroc prawde jezeli ktoras krawedz wierzcholka V1 wskazuje V2
      if(v1[i]->getV2()==&v2)return true; //  zwroc prawde jezeli ktoras krawedz wierzcholka V1 wskazuje V2
    }
    return false;
  }
}

template<class Object,class Connect>
void graf<Object,Connect>::replace(vertex<Object,Connect>& v,Object x){
  v.getZawartosc()=x; //  podmiana wartosci
}

template<class Object,class Connect>
void graf<Object,Connect>::replace(edge<Object,Connect>& e,Connect x){
  e.getZawartosc()=x; //  podmiana wartosci
}

