#include<iostream>
#include <string>
#include <cstdlib>
#include "graf.hh"
using namespace std;

int dl_kolejki=0;

int main(int argc, char *argv[]){
  graf<int,int> grafM(1);
  graf<int,int> graf(0);
  srand(time(NULL));
  cout<<endl<<"Implementacja na liscie sasiedztwa"<<endl<<endl;
  cout<<"Dodanie wierzcholkow: 5 8 1 4"<<endl;
  cout<<"Dodanie krawedzi miedzy 5 i 8, 5 i 4, 1 i 8, 1 i 4"<<endl;
  graf.insertVertex(5);
  graf.insertVertex(8);
  graf.insertVertex(1);
  graf.insertVertex(4);
  graf.insertEdge(graf[0],graf[1],10);
  graf.insertEdge(graf[0],graf[3],11);
  graf.insertEdge(graf[2],graf[1],10);
  graf.insertEdge(graf[2],graf[3],11);
  cout<<"Krawedzie grafu";
  graf.edges();
  cout<<"Wierzcholki grafu"<<endl;
  graf.vertices();
  cout<<endl<<"Koncowe wezly krawedzi 5-8:  ";
  cout<<graf.endVerticies(graf(0),0);
  cout<<" "<<graf.endVerticies(graf(0),1);
  cout<<endl<<"Przylegle krawedzie do wierzcholka 5: ";
  cout<<"Ilosc przyleglych krawedzi do wierzcholka 5: "<<graf.incidentEdges(graf[0])<<endl;
  cout<<"przeciwny wierzcholek do "<<graf[0]<<"wzgledem "<<(*graf[0][0]).getZawartosc()<<" to: "<<graf.opposite(graf[0],(*graf[0][0]))<<endl;
  cout<<"Krawedzie grafu";
  if(!graf.edges())cout<<"Nie ma krawedzi"<<endl;
  cout<<"Wierzcholki grafu"<<endl;
  if(!graf.vertices())cout<<"Nie ma wierzcholkow"<<endl;
  cout<<endl<<"Usuwana krawedz: "<<graf.removeEdge(graf(0))<<endl;
  cout<<"Krawedzie grafu";
  if(!graf.edges())cout<<"Nie ma krawedzi"<<endl;
  cout<<"Wierzcholki grafu"<<endl;
  if(!graf.vertices())cout<<"Nie ma Wierzcholtow"<<endl;
  cout<<endl<<"Usuwany wierzcholek: "<<graf.removeVertex(graf[2])<<endl;
  cout<<"Krawedzie grafu";
  if(!graf.edges())cout<<"Nie ma krawedzi"<<endl;
  cout<<"Wierzcholki grafu"<<endl;
  if(!graf.vertices())cout<<"Nie ma Wierzcholtow"<<endl;
  cout<<endl<<"Szukany wierzcholek: "<<graf.findVertex(5)<<endl;
  cout<<"Szukana krawedz: "<<graf.findEdge(11)<<endl;
  cout<<"5 i 4 sa sasiadami: "<<graf.areAdjacent(graf[0],graf[2])<<endl; //  1 jezeli sa lub 0 jezeli nie sa sasiadami
  cout<<"4 i 5 sa sasiadami: "<<graf.areAdjacent(graf[2],graf[0])<<endl; //  1 jezeli sa lub 0 jezeli nie sa sasiadami
  cout<<"4 i 8 sa sasiadami: "<<graf.areAdjacent(graf[2],graf[1])<<endl; //  1 jezeli sa lub 0 jezeli nie sa sasiadami
  cout<<"Zmiana wierzcholka 5 na 20 i krawedzi 11 na 100"<<endl;
  graf.replace(graf.findEdge(11),100);
  graf.replace(graf.findVertex(5),20);
  cout<<"Krawedzie grafu"<<endl;
  if(!graf.edges())cout<<"Nie ma krawedzi"<<endl;
  cout<<"Wierzcholki grafu"<<endl;
  if(!graf.vertices())cout<<"Nie ma Wierzcholtow"<<endl;
  cout<<endl;
  graf.clear();
  cout<<endl<<"Implementacja na macierzy sasiedztwa"<<endl<<endl;
  cout<<"Dodanie wierzcholkow: 5 8 1 4"<<endl;
  grafM.insertVertex(5);
  grafM.insertVertex(8);
  grafM.insertVertex(1);
  grafM.insertVertex(4);
  cout<<"Dodanie krawedzi miedzy 5 i 8, 5 i 4, 1 i 8, 1 i 4"<<endl;
  grafM.insertEdge(grafM[0],grafM[1],10);
  grafM.insertEdge(grafM[0],grafM[3],11);
  grafM.insertEdge(grafM[2],grafM[1],10);
  grafM.insertEdge(grafM[2],grafM[3],11);
  cout<<"Krawedzie grafu"<<endl;
  grafM.edges();
  cout<<"Wierzcholki grafu"<<endl;
  grafM.vertices();
  cout<<endl<<"Koncowe wezly krawedzi 5-8: ";
  cout<<grafM.endVerticies(grafM(1,0),0);
  cout<<grafM.endVerticies(grafM(1,0),1);
  cout<<endl<<"Przylegle krawedzie do wierzcholka 5: ";
  cout<<"Ilosc przyleglych krawedzi do wierzcholka 5: "<<grafM.incidentEdges(grafM[0])<<endl;
  cout<<"przeciwny wierzcholek do "<<grafM[0]<<"wzgledem "<<(grafM(1,0)).getZawartosc()<<" to: "<<grafM.opposite(grafM[0],(grafM(1,0)))<<endl;
  cout<<"Krawedzie grafu"<<endl;
  if(!grafM.edges())cout<<"Nie ma krawedzi"<<endl;
  cout<<"Wierzcholki grafu"<<endl;
  if(!grafM.vertices())cout<<"Nie ma wierzcholkow"<<endl;
  cout<<endl<<"Usuwana krawedz: "<<grafM.removeEdge(grafM(1,0))<<endl;
  cout<<"Krawedzie grafu"<<endl;
  if(!grafM.edges())cout<<"Nie ma krawedzi"<<endl;
  cout<<"Wierzcholki grafu"<<endl;
  if(!grafM.vertices())cout<<"Nie ma Wierzcholtow"<<endl;
  cout<<endl<<"Usuwany wierzcholek: ";
  cout<<grafM.removeVertex(grafM[2])<<endl;
  cout<<"Krawedzie grafu"<<endl;
  if(!grafM.edges())cout<<"Nie ma krawedzi"<<endl;
  cout<<"Wierzcholki grafu"<<endl;
  if(!grafM.vertices())cout<<"Nie ma Wierzcholtow"<<endl;
  cout<<endl<<"Szukany wierzcholek: "<<grafM.findVertex(5)<<endl;
  cout<<"Szukana krawedz: "<<grafM.findEdge(11)<<endl;
  cout<<grafM[0]<<"i "<<grafM[2]<<"sa sasiadami: "<<grafM.areAdjacent(grafM[0],grafM[2])<<endl; //  1 jezeli sa lub 0 jezeli nie sa sasiadami
  cout<<grafM[2]<<"i "<<grafM[0]<<"sa sasiadami: "<<grafM.areAdjacent(grafM[2],grafM[0])<<endl; //  1 jezeli sa lub 0 jezeli nie sa sasiadami
  cout<<grafM[2]<<"i "<<grafM[1]<<"sa sasiadami: "<<grafM.areAdjacent(grafM[2],grafM[1])<<endl; //  1 jezeli sa lub 0 jezeli nie sa sasiadami
  cout<<"Zmiana wierzcholka 5 na 20 i krawedzi 11 na 100"<<endl;
  grafM.replace(grafM.findVertex(5),20);
  grafM.replace(grafM.findEdge(11),100);
  cout<<"Krawedzie grafu"<<endl;
  if(!grafM.edges())cout<<"Nie ma krawedzi"<<endl;
  cout<<"Wierzcholki grafu"<<endl;
  if(!grafM.vertices())cout<<"Nie ma Wierzcholtow"<<endl;
  cout<<endl;
  grafM.clear();
}
