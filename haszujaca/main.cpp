#include<iostream>
#include <string>
#include <cstdlib>
#include "haszujaca.hh"

using namespace std;

int dl_kolejki=0;

bool menu(int dlTablicy,char rodzajTablicy); //  funkcja w któ©ej jest powolywana tablica haszujaca i w ktorej sa wywolywane funkcje

template <class Object>
void wypelnij(haszujaca<Object>& tablica,int zakres); //  wypelnia tablice elementami z zakresu 0-zakres i w ilosci dlugosci tablicy

bool czyPierwsza(int N); //  jezeli N jest liczba pierwsza to zwraca true

int main(int argc, char *argv[]){
  int dlTablicy;
  char rodzajTablicy;
  bool end=true;
  do{
    while(end){
      cout<<endl<<"Lista haszujaca z linkowaniem"<<endl<<endl;
      dlTablicy=11;
      rodzajTablicy='0';
      if(((rodzajTablicy=='0')||(rodzajTablicy=='1')||(rodzajTablicy=='2'))&&czyPierwsza(dlTablicy))end=false; //  jezeli wybrana tablica jest poprawna to przejsc do menu()
      else cout<<"Zle okreslono parametry tablicy."<<endl<<"Pamietaj ze rozmiar tablicy musi byc liczba pierwsza"<<endl; // jeżeli nie wybrano poprawnie to wybierz ponownie
    }
    end=true;
  }while(menu(dlTablicy,rodzajTablicy)); // dopoki menu zwraca 1 to jest funkcja zapetlona
  do{
    while(end){
      cout<<endl<<"Lista haszujaca z liniowaniem"<<endl<<endl;
      dlTablicy=11;
      rodzajTablicy='1';
      if(((rodzajTablicy=='0')||(rodzajTablicy=='1')||(rodzajTablicy=='2'))&&czyPierwsza(dlTablicy))end=false; //  jezeli wybrana tablica jest poprawna to przejsc do menu()
      else cout<<"Zle okreslono parametry tablicy."<<endl<<"Pamietaj ze rozmiar tablicy musi byc liczba pierwsza"<<endl; // jeżeli nie wybrano poprawnie to wybierz ponownie
    }
    end=true;
  }while(menu(dlTablicy,rodzajTablicy)); // dopoki menu zwraca 1 to jest funkcja zapetlona
  do{
    while(end){
      cout<<endl<<"Lista haszujaca z druga lista haszujaca"<<endl<<endl;
      dlTablicy=11;
      rodzajTablicy='2';
      if(((rodzajTablicy=='0')||(rodzajTablicy=='1')||(rodzajTablicy=='2'))&&czyPierwsza(dlTablicy))end=false; //  jezeli wybrana tablica jest poprawna to przejsc do menu()
      else cout<<"Zle okreslono parametry tablicy."<<endl<<"Pamietaj ze rozmiar tablicy musi byc liczba pierwsza"<<endl; // jeżeli nie wybrano poprawnie to wybierz ponownie
    }
    end=true;
  }while(menu(dlTablicy,rodzajTablicy)); // dopoki menu zwraca 1 to jest funkcja zapetlona
}

bool menu(int dlTablicy,char rodzajTablicy){
  haszujaca<int> tablica(dlTablicy,rodzajTablicy);
  int kroki;
  cout<<"Dodajemy elementy: 1 16 7 9 3 2 2 9 5 6 7 55"<<endl;
  kroki=tablica.dodaj(1);
  if(kroki)cout<<"Dodajac element wykonano "<<kroki<<" krokow."<<endl;
  else cout<<"Nie dodano elementu"<<endl;
  kroki=tablica.dodaj(16);
  if(kroki)cout<<"Dodajac element wykonano "<<kroki<<" krokow."<<endl;
  else cout<<"Nie dodano elementu"<<endl;
  kroki=tablica.dodaj(7);
  if(kroki)cout<<"Dodajac element wykonano "<<kroki<<" krokow."<<endl;
  else cout<<"Nie dodano elementu"<<endl;
  kroki=tablica.dodaj(9);
  if(kroki)cout<<"Dodajac element wykonano "<<kroki<<" krokow."<<endl;
  else cout<<"Nie dodano elementu"<<endl;
  kroki=tablica.dodaj(3);
  if(kroki)cout<<"Dodajac element wykonano "<<kroki<<" krokow."<<endl;
  else cout<<"Nie dodano elementu"<<endl;
  kroki=tablica.dodaj(2);
  if(kroki)cout<<"Dodajac element wykonano "<<kroki<<" krokow."<<endl;
  else cout<<"Nie dodano elementu"<<endl;
  kroki=tablica.dodaj(2);
  if(kroki)cout<<"Dodajac element wykonano "<<kroki<<" krokow."<<endl;
  else cout<<"Nie dodano elementu"<<endl;
  kroki=tablica.dodaj(9);
  if(kroki)cout<<"Dodajac element wykonano "<<kroki<<" krokow."<<endl;
  else cout<<"Nie dodano elementu"<<endl;
  kroki=tablica.dodaj(5);
  if(kroki)cout<<"Dodajac element wykonano "<<kroki<<" krokow."<<endl;
  else cout<<"Nie dodano elementu"<<endl;
  kroki=tablica.dodaj(6);
  if(kroki)cout<<"Dodajac element wykonano "<<kroki<<" krokow."<<endl;
  else cout<<"Nie dodano elementu"<<endl;
  kroki=tablica.dodaj(7);
  if(kroki)cout<<"Dodajac element wykonano "<<kroki<<" krokow."<<endl;
  else cout<<"Nie dodano elementu"<<endl;
  kroki=tablica.dodaj(55);
  if(kroki)cout<<"Dodajac element wykonano "<<kroki<<" krokow."<<endl;
  else cout<<"Nie dodano elementu"<<endl;

  cout<<endl;
  tablica.screen();
  cout<<endl;

  cout<<"Usuwamy: 5 7 98"<<endl;
  try{
    kroki=tablica.usun(5);
    if(kroki)cout<<"Usuwajac element wykonano "<<kroki<<" krokow."<<endl;
    else cout<<"Element 5 nie istnieje"<<endl;
  }catch(haszTabEmptyException){
    cout<<"Element 5 nie istnieje"<<endl;
  }
  try{
    kroki=tablica.usun(7);
    if(kroki)cout<<"Usuwajac element wykonano "<<kroki<<" krokow."<<endl;
    else cout<<"Element 7 nie istnieje"<<endl;
  }catch(haszTabEmptyException){
    cout<<"Element 7 nie istnieje"<<endl;
  }
  try{
    kroki=tablica.usun(98);
    if(kroki)cout<<"Usuwajac element wykonano "<<kroki<<" krokow."<<endl;
    else cout<<"Element 98 nie istnieje"<<endl;
  }catch(haszTabEmptyException){
    cout<<"Element 98 nie istnieje"<<endl;
  }
  
  cout<<endl;
  tablica.screen();
  cout<<endl;

  cout<<"Szukamy 7 i 12"<<endl;
  try{
    kroki=tablica.szukaj(7);
    if(rodzajTablicy=='0')cout<<"Pozycja elementu lista  "<<7%dlTablicy<<" oraz element listy nr: "<<kroki<<endl;
    else cout<<"Pozycja elementu: "<<kroki<<endl;
  }catch(haszTabEmptyException){
    cout<<"Wskazany element nie istnieje"<<endl;
  }
  try{
    kroki=tablica.szukaj(12);
    if(rodzajTablicy=='0')cout<<"Pozycja elementu lista  "<<12%dlTablicy<<" oraz element listy nr: "<<kroki<<endl;
    else cout<<"Pozycja elementu: "<<kroki<<endl;
  }catch(haszTabEmptyException){
    cout<<"Wskazany element nie istnieje"<<endl;
  }
  cout<<"Czyscimy tablice i wypelniamy losowymi wartosciami"<<endl;
  tablica.clear();
  wypelnij(tablica,20);
  cout<<endl;
  tablica.screen();
  cout<<endl;
  cout<<"Usuwamy: 5 7 98"<<endl;
  try{
    kroki=tablica.usun(5);
    if(kroki)cout<<"Usuwajac element wykonano "<<kroki<<" krokow."<<endl;
    else cout<<"Element 5 nie istnieje"<<endl;
  }catch(haszTabEmptyException){
    cout<<"Element 5 nie istnieje"<<endl;
  }
  try{
    kroki=tablica.usun(7);
    if(kroki)cout<<"Usuwajac element wykonano "<<kroki<<" krokow."<<endl;
    else cout<<"Element 7 nie istnieje"<<endl;
  }catch(haszTabEmptyException){
    cout<<"Element 7 nie istnieje"<<endl;
  }
  try{
    kroki=tablica.usun(98);
    if(kroki)cout<<"Usuwajac element wykonano "<<kroki<<" krokow."<<endl;
    else cout<<"Element 98 nie istnieje"<<endl;
  }catch(haszTabEmptyException){
    cout<<"Element 98 nie istnieje"<<endl;
  }
  
  cout<<endl;
  tablica.screen();
  cout<<endl;

  cout<<"Szukamy 7 i 12"<<endl;
  try{
    kroki=tablica.szukaj(7);
    if(rodzajTablicy=='0')cout<<"Pozycja elementu lista  "<<7%dlTablicy<<" oraz element listy nr: "<<kroki<<endl;
    else cout<<"Pozycja elementu: "<<kroki<<endl;
  }catch(haszTabEmptyException){
    cout<<"Wskazany element nie istnieje"<<endl;
  }
  try{
    kroki=tablica.szukaj(12);
    if(rodzajTablicy=='0')cout<<"Pozycja elementu lista  "<<12%dlTablicy<<" oraz element listy nr: "<<kroki<<endl;
    else cout<<"Pozycja elementu: "<<kroki<<endl;
  }catch(haszTabEmptyException){
    cout<<"Wskazany element nie istnieje"<<endl;
  }
  return 0;
}

template <class Object>
void wypelnij(haszujaca<Object>& tablica,int zakres){
  doubleList<Object> lista; // lista elementow
  int kroki;
  Object elem;
  bool doIt=true;
  srand(time(NULL));
  if(zakres<tablica.rozmiar())zakres=tablica.rozmiar(); //  jezeli zakres jest za maly
  while(lista.rozmiar()!=tablica.rozmiar()){
    elem=rand()%zakres;
    for(int j=0;j<lista.rozmiar();j++){
      if(elem==lista[j])doIt=false; //  sprawdzanie czy juz takie element istnieje
    }
    if(doIt)kroki=lista.wstawOstatni(elem);
    doIt=true;
  }
  while(!lista.isEmpty()){ //  dodawanie do tablicy haszujacej
    elem=lista.usunPierwszy();
    kroki=tablica.dodaj(elem);
    if(kroki)cout<<"Dodajac element "<<elem<<" wykonano: "<<kroki<<" krokow."<<endl;    
    else cout<<"Nie dodano elementu"<<endl;
  }
  lista.clear();
}

bool czyPierwsza(int N){
  int f=(N+1)/2;
  bool pierwsze[N+1];
  if((N==0)||(N==1))return false; //  0 i 1 to nie liczby pierwsze
  for(int i=0;i<(N+1);i++)pierwsze[i]=1; //  przypisywanie wszystkim elementom wartosci true
  for(int i=2;i<f;i++)if(pierwsze[i])for(int j=i+i;j<(N+1);j+=i)pierwsze[j]=0; //  przypisywanie wszystkim elementom nie pierwszym wartosci 0
  if(pierwsze[N])return true;
  return false;
}
