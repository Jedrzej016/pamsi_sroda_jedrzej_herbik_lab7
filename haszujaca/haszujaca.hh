#include "doubleList.hh"
class haszTabEmptyException{};
class doNotTouch{};

template <class Object>
class haszElem{
  Object wartosc;
  bool pusty;
  bool oprozniony;
public:
  Object& operator =(Object& a)throw(doNotTouch){if(pusty)throw doNotTouch();wartosc=a;return wartosc;}
  Object& operator +(Object& a)throw(doNotTouch){if(pusty)throw doNotTouch();return (wartosc+a);}
  Object& operator -(Object& a)throw(doNotTouch){if(pusty)throw doNotTouch();return (wartosc-a);}
  Object& operator *(Object& a)throw(doNotTouch){if(pusty)throw doNotTouch();return (wartosc*a);}
  Object& operator /(Object& a)throw(doNotTouch){if(pusty)throw doNotTouch();return (wartosc/a);}
  bool operator ==(Object& a)throw(doNotTouch){if(pusty)throw doNotTouch();if(wartosc==a)return true;return false;}
  bool operator >=(Object& a)throw(doNotTouch){if(pusty)throw doNotTouch();if(wartosc>=a)return true;return false;}
  bool operator <=(Object& a)throw(doNotTouch){if(pusty)throw doNotTouch();if(wartosc<=a)return true;return false;}
  bool operator >(Object& a)throw(doNotTouch){if(pusty)throw doNotTouch();if(wartosc>a)return true;return false;}
  bool operator <(Object& a)throw(doNotTouch){if(pusty)throw doNotTouch();if(wartosc<a)return true;return false;}
  Object& getWartosc(){return wartosc;}
  Object& setWartosc(){pusty=false;oprozniony=false;return wartosc;}
  bool getPusty(){return pusty;}
  bool getOprozniony(){return oprozniony;}
  void clear(){pusty=true;oprozniony=true;wartosc=0;}
  haszElem(){pusty=true;oprozniony=false;}
};

template <class Object> // szablon kolejki priorytetowej
class haszujaca{  // dziedziczenie po liscie dwukierunkowej ktorej elementami sa zawartoscKolejkiPrio
  doubleList<Object> *dane;
  haszElem<Object> *dane2;
  int dlugosc;
  int size;
  char typTablicyHaszujacej;
  int pierwszaMax;
  int pierwszaMax2;
  void destruct();
  int fhaszujaca(const int wartosc);
  int f2haszujaca(const int wartosc,int wywolanie);
public:
  haszujaca(int N){dlugosc=N;dane=new doubleList<Object>[dlugosc];typTablicyHaszujacej=0;}
  haszujaca(int N,char sposob);
  ~haszujaca(){this->destruct();}
  int rozmiar(){return dlugosc;}
  int iloscElementow(){return size;}
  int dodaj(Object wartosc);
  int szukaj(Object wartosc)throw(haszTabEmptyException);
  int usun(Object wartosc)throw(haszTabEmptyException);
  void czysc();
  bool clear();
  bool screen();
};

template <class Object>
int haszujaca<Object>::fhaszujaca(const int wartosc){
  return (wartosc%dlugosc);
}

template <class Object>
int haszujaca<Object>::f2haszujaca(const int wartosc,int wywolanie){
  return (fhaszujaca(wartosc)+wywolanie*(pierwszaMax2-wartosc%pierwszaMax2))%dlugosc;
}

template <class Object>
haszujaca<Object>::haszujaca(int N,char sposob){
  int f=(N+1)/2;
  bool pierwsze[N+1];
  for(int i=0;i<(N+1);i++)pierwsze[i]=1;
  for(int i=2;i<f;i++)if(pierwsze[i])for(int j=i+i;j<(N+1);j+=i)pierwsze[j]=0;
  for(int i=2;i<(N+1);i++)if(pierwsze[i])pierwszaMax=i;
  pierwsze[pierwszaMax]=0;
  for(int i=2;i<(N+1);i++)if(pierwsze[i])pierwszaMax2=i;
  typTablicyHaszujacej=sposob;
  dlugosc=N;
  if(dlugosc!=pierwszaMax){
    std::cout<<"Podana dlugosc tablicy nie jest liczba pierwsza"<<std::endl;
    this->destruct();
  }
  if(sposob=='0'){
    dane=new doubleList<Object>[dlugosc];
    dane2=NULL;
  }else if(sposob=='1'){
    dane=NULL;
    dane2=new haszElem<Object>[dlugosc];
  }else if(sposob=='2'){
    dane=NULL;
    dane2=new haszElem<Object>[dlugosc];
  }else{
    std::cout<<"Zle okreslony typ rozwiazywaniakolizji"<<std::endl<<"Tablica nie zostala utworzona"<<std::endl;
  }
}

template <class Object>
int haszujaca<Object>::dodaj(Object wartosc){
  int kroki=1;
  int tmp=0;
  int haszowanaWartosc=fhaszujaca(wartosc);
  if(typTablicyHaszujacej=='0'){
    dane[haszowanaWartosc].wstawOstatni(wartosc);
    return 1;
  }else if(typTablicyHaszujacej=='1'){
    tmp=haszowanaWartosc;
    while(1){
      if(dane2[haszowanaWartosc].getPusty()){
	dane2[haszowanaWartosc].setWartosc()=wartosc;
	return kroki;      
      }
      ++haszowanaWartosc;
      haszowanaWartosc=haszowanaWartosc%dlugosc;
      kroki++;
      if(haszowanaWartosc==tmp)return 0;
    }
  }else if(typTablicyHaszujacej=='2'){
    tmp=haszowanaWartosc;
    while(1){
      if(dane2[haszowanaWartosc].getPusty()){
	dane2[haszowanaWartosc].setWartosc()=wartosc;
	return kroki;      
      }
      haszowanaWartosc=f2haszujaca(wartosc,kroki);
      kroki++;
      if(haszowanaWartosc==tmp)return 0;
    }    
  }
  return wartosc;
}

template <class Object>
int haszujaca<Object>::usun(Object wartosc)throw(haszTabEmptyException){
  doubleListInside<Object> *wskn=NULL;
  doubleListInside<Object> *wskb=NULL;
  int kroki=1;
  int nr=fhaszujaca(wartosc);
  int tmp=0;
  if(typTablicyHaszujacej=='0'){
    wskn=dane[nr].getNext();
    wskb=dane[nr].getBack();
    if(wskn==NULL)throw haszTabEmptyException();
    tmp=1;
    while(1){
      if(wskn->getZawartosc()==wartosc){
	if(wskn==dane[nr].getNext()){
	  dane[nr].usunPierwszy();
	  return tmp;      
	}else if(wskn==dane[nr].getBack()){
	  dane[nr].usunOstatni();
	  return tmp;      
	}else{
	  wskb=wskn->getBack();
	  wskn=wskn->getNext();
	  delete(wskn->getBack());
	  wskb->getNext()=wskn;
	  wskn->getBack()=wskb;	  
	  return tmp;      
	}
      }
      if(wskn->getNext()!=NULL)wskn=wskn->getNext();
      else throw haszTabEmptyException();
      tmp++;
    }
  }else if(typTablicyHaszujacej=='1'){
    tmp=nr;
    if(dane2[nr].getPusty())if(!dane2[nr].getOprozniony())throw haszTabEmptyException();
    while(1){
      if(!dane2[nr].getPusty())if(dane2[nr]==wartosc){
	  dane2[nr].clear();
	  return kroki;
	}
      nr++;
      nr=nr%dlugosc;
      kroki++;
      if(nr==tmp)return 0;
      if(dane2[nr].getPusty())if(!dane2[nr].getOprozniony())throw haszTabEmptyException();
    }
  }else if(typTablicyHaszujacej=='2'){
    tmp=nr;
    if(dane2[nr].getPusty())if(!dane2[nr].getOprozniony())throw haszTabEmptyException();
    while(1){
      if(!dane2[nr].getPusty())if(dane2[nr]==wartosc){
	  dane2[nr].clear();
	  return kroki;
	}
      nr=f2haszujaca(wartosc,kroki);
      kroki++;
      if(nr==tmp)return 0;
      if(dane2[nr].getPusty())if(!dane2[nr].getOprozniony())throw haszTabEmptyException();
    }
  }
  return 0;
}

template <class Object>
int haszujaca<Object>::szukaj(Object wartosc)throw(haszTabEmptyException){
  doubleListInside<Object> *wskn=NULL;
  int kroki=1;
  int nr=fhaszujaca(wartosc);
  int tmp=0;
  if(typTablicyHaszujacej=='0'){
    wskn=dane[nr].getNext();
    if(wskn==NULL)throw haszTabEmptyException();
    while(1){
      if(wskn->getZawartosc()==wartosc){
	std::cout<<"Szukajac elementu wykonano "<<kroki<<" krokow"<<std::endl;
	return tmp;      
      }
      if(wskn->getNext()!=NULL)wskn=wskn->getNext();
      else throw haszTabEmptyException();
      tmp++;
      kroki++;
    }
  }else if(typTablicyHaszujacej=='1'){
    tmp=nr;
    if(dane2[nr].getPusty())if(!dane2[nr].getOprozniony())throw haszTabEmptyException();
    while(1){
      if(!dane2[nr].getPusty())if(dane2[nr]==wartosc){
	  std::cout<<"Szukajac elementu wykonano "<<kroki<<" krokow"<<std::endl;
	  return nr;
	}
      nr++;
      nr=nr%dlugosc;
      kroki++;
      if(nr==tmp)throw haszTabEmptyException();;
      if(dane2[nr].getPusty())if(!dane2[nr].getOprozniony())throw haszTabEmptyException();
    }
  }else if(typTablicyHaszujacej=='2'){
    tmp=nr;
    if(dane2[nr].getPusty())if(!dane2[nr].getOprozniony())throw haszTabEmptyException();
    while(1){
      if(!dane2[nr].getPusty())if(dane2[nr]==wartosc){
	  std::cout<<"Szukajac elementu wykonano "<<kroki<<" krokow"<<std::endl;
	  return nr;
	}
      nr=f2haszujaca(wartosc,kroki);
      kroki++;
      if(nr==tmp)throw haszTabEmptyException();;
      if(dane2[nr].getPusty())if(!dane2[nr].getOprozniony())throw haszTabEmptyException();
    }
  }
  return 0;
}

template <class Object>
void haszujaca<Object>::czysc(){
  if(typTablicyHaszujacej=='0'){
    for(int i=0;i<dlugosc;i++){
      size=size-dane[i].rozmiar();
      dane[i].clear();
    }
  }else{
    for(int i=0;i<dlugosc;i++){
      dane2[i].clear();
    }    
  }
}

template <class Object>
void haszujaca<Object>::destruct(){
  if(typTablicyHaszujacej=='0'){
    this->clear();//std::cout<<"WYCZYSZCZONO WSKAZNIKI"<<std::endl;
    delete[](dane);
  }else{
    delete[](dane2);
  }
}

template <class Object>
bool haszujaca<Object>::clear(){
  if(typTablicyHaszujacej=='0'){
    for(int i=0;i<dlugosc;i++){
      size=size-dane[i].rozmiar();
      dane[i].clear();
    }
  }else{
    for(int i=0;i<dlugosc;i++){
      dane2[i].clear();
    }    
  }
  if(dlugosc)return false;
  return true;
}

template <class Object>
bool haszujaca<Object>::screen(){
  if(typTablicyHaszujacej=='0'){
    for(int i=0;i<dlugosc;i++){
      dane[i].screen();
      std::cout<<std::endl;
    }
  }else{
    for(int i=0;i<dlugosc;i++){
      if(dane2[i].getPusty())std::cout<<"E ";
      else std::cout<<dane2[i].getWartosc()<<" ";
    }
    std::cout<<std::endl;
  }
  return true;
}